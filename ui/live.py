import datetime
import uuid

from collections import OrderedDict

MAX_TABLE_ENTRIES = 800

class LiveCursor:
    def __init__(self, db):
        self.db = db

    def execute(self, query):
        SELECT = "select * from "
        WHERE = " where id_experiment = Live"
        if (not query.startswith(SELECT) or
            "where" in query and not query.endswith(WHERE)):
            raise Exception("Query not supported in live mode: {}".format(query))

        self.query = query

        table_name = query.replace(SELECT, "").replace(WHERE, "")

        if table_name == "experiments":
            self.data = [list(self.db.current.parameters.values())]
            self._fields = list(self.db.current.parameters.keys())
        else:
            self.data = [list(entry.values())
                         for entry in self.db.current.tables[table_name]]

            self._fields = list(self.db.current.tables[table_name][0].keys()) if self.data else None

    def fetchall(self):
        return self.data

class LiveState:
    def __init__(self):
        self.parameters = OrderedDict(
            id="Live",
            date=datetime.datetime.now().strftime("%Y-%d-%d %H:%M:%S"),
            description="Live plotting",
            fps="(fps)",
            width="(width)", height="(height)",
            gop="(gop)",
            bitrate="(bitrate)",
            num_ref_frames="(num ref frames)",
            uuid=str(uuid.uuid4()),
            imported="0")

        import experiment # cannot be imported at top level because of cyclic dependency
        self.tables = dict([(tbl_name, []) for tbl_name in experiment.get_all_tables()])
        self.tables["attachments"] = []

        self.tables_new_records = dict(self.tables)

class LiveDatabaseClass:
    def __init__(self):
        self.current = LiveState()
        self.next = LiveState()
        self.ids = dict([(tbl_name, 1) for tbl_name in self.next.tables.keys()])

    def rollback(self):
        pass

    def cursor(self):
        return LiveCursor(self)

    def new_experiment(self, parameters):
        self.next.parameters.update(parameters)

    def save_table(self, table_name, field_names, row_generator):
        old_records = self.current.tables[table_name]

        new_records = []
        # add new records
        for row in row_generator:
            entry = OrderedDict()

            entry["id"] = self.ids[table_name]
            self.ids[table_name] += 1

            for key, value in zip(field_names, row):
                entry[key] = value
            new_records.append(entry)

        # truncate the table to the last MAX_TABLE_ENTRIES entries
        self.next.tables[table_name] = (old_records + new_records)[-MAX_TABLE_ENTRIES:]

        self.next.tables_new_records[table_name] = new_records

    def commit(self):
        for key, value in self.current.parameters.items():
            if key in self.next.parameters: continue
            self.next.parameters[key] = value

        self.next.parameters["imported"] = int(self.next.parameters["imported"]) + 1
        # must be atomic
        self.current, self.next = self.next, LiveState()

        # trigger refresh of the current graph
        try:
            import ui.dataview
            if ui.dataview.GraphDataView.remap_current:
                ui.dataview.GraphDataView.remap_current()
        except Exception as e:
            print("WARNING: error while plotting the data in the UI ...")
            print(e.__class__.__name__+":", e)

        try:
            process_new_records(self.current.tables_new_records)
        except Exception as e:
            print("WARNING: error while processing...")
            print(e.__class__.__name__+":", e)

_db = None
def LiveDatabase():
    global _db
    if _db is None:
        _db = LiveDatabaseClass()
    return _db


def process_new_records(tables_new_records):
    return

    for table, records in tables_new_records.items():
        if not records:  continue
        #if table != "sys": continue
        print(table)
        print("-" * len(table))
        first = True
        for record in records:
            if first:
                print(",".join(record.keys()))
                first = False
            print(",".join(map(str, record.values())))
        print("---\n")
    print("-*-\n")
