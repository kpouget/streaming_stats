#!/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk

from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.ticker import AutoMinorLocator
import matplotlib.pyplot as pyplot
import numpy
from scipy.spatial.distance import cdist
import datetime

from collections import defaultdict

import yaml

# will be changed by ExperimentsView when a database is opened
RUNNING_LIVE = False

class Plot(object):

    def __init__(self, x, y, title, y_label=None, x_label='time(s)'):
        self.x = [_x for _x, _y in zip(x, y) if _y is not None]

        self.rel_x = numpy.array(self.x) - self.x[0] if self.x else []
        self.y = [_y for _x, _y in zip(x, y) if _y is not None]

        self.x_label = x_label
        self.y_label = y_label
        self.title = title
    # __init__

    @staticmethod
    def zeroify(_list):
        return [i or 0 for i in _list]
    # zeroify
# Plot

class PlotMetaData(object):

    def __init__(self, x_attr, x_label, y_attr):
        self.x_attr = x_attr
        self.x_label = x_label
        self.y_attr = y_attr

        self.plots = None
    #__init__

    def process_data(self, data):
        if not RUNNING_LIVE and self.plots is not None:
            return self.plots

        if self.plots:
            self.plots = []
            data.refresh(self.x_attr)

        self.plots = []

        try:
            dataset_length = data.length(self.x_attr)
        except Exception as e:
            print(f"WARNING: cannot access X attribute '{self.x_attr}'")
            print(e)
            raise(e)
            return False

        print(f"{dataset_length} items to plot for {self.y_attr[0][0].split('.')[0]}")

        x_data = [data.get(self.x_attr, row_idx) for row_idx in range(dataset_length)]

        for y_attr, title, label in self.y_attr:
            try:
                y_length = data.length(y_attr)
            except Exception:
                print(f"WARNING: cannot access Y attribute '{y_attr}', skipping it")
                print(e)
                continue

            if dataset_length < y_length:
                print(f"WARNING: {self.x_attr} and {y_attr} don't have the same length "
                      f"({dataset_length} and {y_length}), skipping.")
                continue

            y_data = [data.get(y_attr, row_idx) for row_idx in range(dataset_length)]

            self.plots.append(Plot(x_data, y_data, title, label, self.x_label))

        return self.plots
    # process_data
# PlotMetaData

class DataView(object):

    def __init__(self, data):
        self.data = data
    # __init__
# DataView

class GraphDataView(Gtk.ScrolledWindow, DataView):
    remap_current = None

    def __init__(self, data):
        DataView.__init__(self, data)
        Gtk.ScrolledWindow.__init__(self)
        self.graph = None
        self.axes = None
        self.set_label_placeholder("Loading...")
        self.connect("map", self.map_cb)
        self.connect("unmap", self.unmap_cb)
        self.tooltips = {}
        self.plots = {}
    # __init__

    def set_label_placeholder(self, text):
        child = self.get_child()
        if child:
            self.remove(child)
        self.add(Gtk.Label("<i>%s</i>" % text, use_markup=True))
        self.show_all()
    # set_label_placeholder

    def map_cb(self, widget=None):
        if not self.data:
            self.set_label_placeholder("No data provided")
            return

        #print("Tab:", self.text)

        GraphDataView.remap_current = self.map_cb


        try:
            plots = self.metadata.process_data(self.data)
        except Exception as e:
            print("WARNING: error while processing the data for plotting...")
            print(e.__class__.__name__+":", e)
            plots = False

        if not plots:
            self.set_label_placeholder("No graph can be plotted on this tab.")
            return

        def plot_idle():
            nrows = len(plots)
            if not self.graph:

                self.graph, self.axes = pyplot.subplots(nrows=nrows, sharex=True)
                self.graph.subplots_adjust(top=0.95, bottom=0.05, left=0.1, right=0.95, hspace=0.3)
                self.graph.align_ylabels()


            for i in range(nrows):
                try:
                    a = self.axes[i]
                except IndexError:
                    print(f"{self.text}: Cannot find axis {i}/{nrows}")
                    continue

                p = plots[i]
                a.clear()
                self.plots[a] = p
                a.set(title=p.title, ylabel=p.y_label)
                if i == nrows-1:
                    a.set_xlabel(p.x_label)

                a.grid(which='both', linestyle=":")
                a.get_xaxis().set_minor_locator(AutoMinorLocator())
                a.plot(p.rel_x, p.y, '.-', picker=5)

            canvas = FigureCanvas(self.graph)
            canvas.set_size_request(900, 700)
            canvas.mpl_connect('pick_event', self.update_tooltip)

            vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            vbox.pack_start(canvas, True, True, 0)
            toolbar = NavigationToolbar(canvas, self.get_toplevel())
            vbox.pack_start(toolbar, False, True, 0)
            child = self.get_child()
            if child:
                self.remove(child)

            self.add_with_viewport(vbox)
            self.show_all()
            return False
        # plot_idle

        GLib.idle_add(plot_idle)
    # map_cb

    def unmap_cb(self, widget):
        if self.graph:
            pyplot.close(self.graph)
            self.tooltips = {}
        self.set_label_placeholder("Loading...")
    # unmap_cb

    def update_tooltip(self, event):
        line = event.artist
        event_xy = (event.mouseevent.xdata, event.mouseevent.ydata)
        xydata = line.get_xydata()

        ### FIXME
        # Pick closest valid point from click event
        # https://codereview.stackexchange.com/questions/28207/finding-the-closest-point-to-a-list-of-points
        idx = cdist([event_xy], xydata).argmin()
        ###

        x, y = xydata[idx]
        axes = line.axes
        x_label = self.plots[axes].x_label
        y_label = self.plots[axes].y_label
        txt = f"Index: {idx}\nRelative {x_label}: {x:0.5f}\n{y_label}: {y:0.5f}"

        try:
            tooltip = self.tooltips[axes]
            tooltip.xy = x, y
            tooltip.set_text(txt)
        except KeyError:
            tooltip = axes.annotate(txt, xy = (x,y),
                                    textcoords = "offset points", xytext = (-20, 20),
                                    bbox = {"boxstyle" : "round,pad=0.5", "fc" : "aliceblue", "alpha" : 0.9},
                                    arrowprops = {"arrowstyle" : "-|>"})
            tooltip.set_visible(True)
            self.tooltips[axes] = tooltip

        event.canvas.draw()
    # update_tooltip

# GraphDataView

class YamlDataViewType():

    def __init__(self, text, metadata):
        self.text = text
        self.metadata = metadata

        self.obj = None

    def __call__(self, data):
        assert self.obj is None

        self.obj = GraphDataView(data)
        self.obj.text = self.text
        self.obj.metadata = self.metadata
        return self.obj

def get_data_views(filename, views=[]):
    views_cfg = yaml.safe_load(open(filename))

    data_views = []
    for tab_title, content in views_cfg.items():
        if not "x" in content:
            print("WARNING: tab '{}' has no 'x' field. Skipping it.".format(tab_title))
            continue

        if views and tab_title not in views: continue

        x_source, found, x_label = content["x"].partition(", ")

        y_attr = []
        for plot_title, y in content.items():
            if plot_title == "x": continue

            y_source, found, y_desc = y.partition(", ")
            y_attr.append((y_source, plot_title, y_desc))

        data_views.append(YamlDataViewType(tab_title,
                                           PlotMetaData(x_source, x_label, y_attr)))

    return data_views


VIRSH_VM_NAME = "fedora30"

def set_encoder(encoder_name, parameters):
    import os
    import json

    params_str = ";".join(f"{k}:{v}" for k, v in parameters.items() if v)
    json_msg = json.dumps(dict(execute="set-spice",
                               arguments={"guest-encoder": encoder_name,
                                          "guest-encoder-params": params_str}))

    cmd = f"virsh qemu-monitor-command {VIRSH_VM_NAME} '{json_msg}'"
    os.system(cmd)

    append_quality(None, f"ui: Setting '{encoder_name}' encoder.")

quality_text_buffer = None
def append_quality(ts, msg):
    if not quality_text_buffer: return

    end_iter = quality_text_buffer.get_end_iter()
    ts_str = datetime.datetime.fromtimestamp(ts).strftime("%H:%M:%S ") if ts else "--:--:-- "

    quality_text_buffer.insert(end_iter, f"{ts_str}{msg}\n")

    if msg.startswith("guest: encoder:"):
        encoder_name = ":".join(msg.split(":")[2:])
        ControlView.encoder_store.append([encoder_name])
        append_quality(None, f"ui: encoder '{encoder_name}' registered")


set_quality = None
def set_quality_callback(cb):
    global set_quality
    set_quality = cb

def error_box(parent, msg):
    print(msg)
    dialog = Gtk.MessageDialog(text=msg,
                               buttons=Gtk.ButtonsType.OK,
                               message_type=Gtk.MessageType.ERROR)

    dialog.run()
    dialog.destroy()

class ControlView(Gtk.Grid, DataView):
    text = "Control center"
    encoder_store = None

    def __init__(self, data):
        DataView.__init__(self, data)
        Gtk.Grid.__init__(self,
                          border_width=1,
                          column_homogeneous=True,
                          column_spacing=1,
                          row_spacing=1)
        self.insert_column(0)
        self.insert_column(1)
        self.insert_row(0)
        self.insert_row(1)

        self.create_headers(0)

        self.create_quality_textview(1)

        self.create_codec_controls(1)

        self.show_all()

    def create_headers(self, row):
        w = Gtk.Label("<b>Quality indicators</b>",
                      use_markup=True, xalign=0.5)
        self.attach(w, 0, row, 1, 1)

        w = Gtk.Label("<b>Codec controls</b>",
                      use_markup=True, xalign=0.5)

        self.attach(w, 1, row, 2, 1)

    def create_quality_textview(self, row):
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.attach(vbox, 0, row, 1, 1)

        vbox.pack_start(Gtk.Label("<b>Quality to send:</b>",
                                  use_markup=True, xalign=0.5),
                        False, False, 0)

        self.quality_box = Gtk.Entry()
        vbox.pack_start(self.quality_box, False, False, 0)

        def send_button_clicked(button):
            if not set_quality:
                print("No quality callback configured ...")
                return
            quality_msg = self.quality_box.get_text()
            print("sending", quality_msg)
            set_quality(quality_msg)

        self.send_button = Gtk.Button(label="Send!")
        self.send_button.connect("clicked", send_button_clicked)
        vbox.pack_start(self.send_button, False, False, 0)

        vbox.pack_start(Gtk.Label("<b>Quality received:</b>",
                                  use_markup=True, xalign=0.5),
                        False, False, 0)

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        vbox.pack_start(scrolledwindow, True, True, 0)

        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        self.textbuffer.set_text("")

        global quality_text_buffer
        quality_text_buffer = self.textbuffer

        scrolledwindow.add(self.textview)

    def create_codec_controls(self, row):
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        self.attach(vbox, 1, row, 2, 1)

        encoder_label = Gtk.Label("<b>Encoder</b>",
                                  use_markup=True,
                                  xalign=0.5, yalign=0.0)
        vbox.pack_start(encoder_label, False, False, 0)

        params = yaml.safe_load(open("codec_params.yaml"))
        self.encoder_name = None

        def create_combo(lst):
            store = Gtk.ListStore(str)

            [store.append([str(key)]) for key in lst]

            combo = Gtk.ComboBox.new_with_model(store)
            renderer_text = Gtk.CellRendererText()
            combo.pack_start(renderer_text, True)
            combo.add_attribute(renderer_text, "text", 0)

            return combo

        def show_encoder_params(encoder_name):
            for main_key, params in self.param_widgets.items():
                visible = encoder_name.startswith(main_key) or main_key == "all"

                for param_name, param_label, param_box in params:
                    for w in [param_label, param_box]:
                        w.set_visible(visible)


        def on_change(combo):
            tree_iter = combo.get_active_iter()
            if tree_iter is not None:
                model = combo.get_model()
                name = model[tree_iter][0]

                print("-"*10)
                print("Selected:", name)

            self.encoder_name = name
            show_encoder_params(name)

        self.encoder_combo = create_combo([])
        self.encoder_combo.connect("changed", on_change)

        ControlView.encoder_store = self.encoder_combo.get_model()

        vbox.pack_start(self.encoder_combo, False, False, 0)

        #---
        def go_button_clicked(button):
            if not self.encoder_name:
                error_box(self, "No encoder selected ...")
                return

            parameters = {}
            for main_key, params in self.param_widgets.items():
                in_use = self.encoder_name.startswith(main_key) or main_key == "all"
                if not in_use: continue

                for param_name, param_label, param_box in params:
                    if isinstance(param_box, Gtk.Entry):
                        value = param_box.get_text()
                    else: # ComboBox
                        active = param_box.get_active()

                        value = None if active is None else \
                            param_box.get_model()[active][0]

                    parameters[f"{main_key}.{param_name}"] = value
            msg = set_encoder(self.encoder_name, parameters)
            if msg is not None:
                error_box(self, msg)

        self.go_button = Gtk.Button(label="Go!")
        self.go_button.connect("clicked", go_button_clicked)
        vbox.pack_start(self.go_button, False, False, 0)

        #---

        params_label = Gtk.Label("<b>Parameters</b>",
                                  use_markup=True,
                                  xalign=0.5, yalign=0.0)
        vbox.pack_start(params_label, False, False, 0)

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(False)
        scrolledwindow.set_vexpand(True)
        scrolledwindow.set_border_width(10)

        vbox.pack_start(scrolledwindow, True, True, 0)

        params_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        scrolledwindow.add(params_vbox)

        self.param_widgets = defaultdict(list)

        for main_key, main_values in params.items():
            if not main_values: continue
            print(f"## {main_key}")

            for param in main_values:
                if isinstance(param, dict):
                    param, values = list(param.items())[0]
                    if isinstance(values, str):
                        values = values
                else: values = None

                param_label = Gtk.Label(f"<i>{main_key}.{param}:</i>",
                                        use_markup=True, xalign=0.0, yalign=0.0)
                params_vbox.pack_start(param_label, False, False, 0)

                if isinstance(values, list):
                    param_box = create_combo(values)
                else:
                    param_box = Gtk.Entry()
                params_vbox.pack_start(param_box, False, False, 0)

                param_label.set_visible(False)
                param_box.set_visible(False)

                self.param_widgets[main_key] += [(param, param_label, param_box)]

class ExperimentDataView(Gtk.Grid, DataView):
    text = "Experiment"

    def __init__(self, data):
        DataView.__init__(self, data)
        Gtk.Grid.__init__(self,
                          border_width=10,
                          column_homogeneous=True,
                          column_spacing=10,
                          row_spacing=10)

        for i in range(2):
            self.insert_column(i)

        for row, (key, val) in enumerate(data):
            self.display_data(key, val, row)

        self.show_all()

    # __init__

    def display_data(self, text, value, row):
        self.insert_row(row)
        w = Gtk.Label("<b>%s:</b>" % text.capitalize(),
                      use_markup=True,
                      xalign=1.0)
        self.attach(w, 0, row, 1, 1)

        if not value:
            value = "<i>not provided</i>"

        w = Gtk.Label("%s" % value,
                      use_markup=True,
                      xalign=0.0)
        self.attach(w, 1, row, 2, 1)
    # display_data
# ExperimentDataView
