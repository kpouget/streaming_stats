#! /usr/bin/env python3

import argparse
import sys
import threading

import benchmark

def launch_and_wait_gui(views):
    from ui import main_window
    import utils.notify

    # the asyncio loop (run.one) must be in the main thread (at the moment)
    app = main_window.StatsApp()
    app.views = views
    thr = threading.Thread(target=app.run, args=[["./perf_collector.py", "::live"]])

    thr.start()

    cond = utils.notify.conditions["live"][1]
    with cond:
        while not utils.notify.conditions["live"][0]:
            cond.wait()

    return thr, utils.notify.conditions["live"][0]


def wait_gui(thr):
    print()
    print("* Waiting for the GUI ...")
    thr.join()


def main():
    # some arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gui', action='store_true', help='launch GUI live view')
    parser.add_argument('-n', '--dry-run', action='store_true', help='dry live run')

    args = parser.parse_args()

    cfg = dict(measurements=["Perf_Collect"])

    if args.gui:
        gui_thr, do_run = launch_and_wait_gui(None)
    else:
        do_run = True

    if do_run:
        print("Running live!")
        benchmark.run_one(cfg, "live measurements", dry_run=args.dry_run, live=True)

    if args.gui:
        wait_gui(gui_thr)

    sys.exit(0)

if __name__ == "__main__":
    main()
