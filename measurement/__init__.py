import subprocess
import asyncio

class ProcessNotRunningMeasurementException(Exception): pass

class Measurement:
    """This class represent a measurement
    The purpose is to define the steps of the measurement.
    A measurement can be for instance how much CPU or memory is
    consumed.
    """
    def __init__(self, experiment):
        """Initialization and possible initial base checks.
        If you need more expensive setup you probably should write it
        in setup() so to allow user to have a faster feedback before
        leaving the keyboard"""
        self.experiment = experiment

    def setup(self):
        """Setup the measurement
        In this step you should launch any tool needed (like a CPU
        monitor) or save any measurement (like disk space)."""
        pass

    def start(self):
        """Start the measurement.
        start and stop are separate to allow to quickly start and
        stop all measurement.
        Do not do too expensive operation"""
        pass

    def stop(self):
        """Stop the measurement.
        See start"""
        pass

    def collect(self):
        """Collect the measurement.
        For instance get logs and parse them.
        Should store the results on a table."""
        pass

    live = None
    def process_line(self, line):
        pass

class MeasurementTemplate(Measurement):
    def __init__(self, cfg, experiment):
        Measurement.__init__(self, experiment)
        pass

    def setup(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def collect(self):
        pass

async def stream_as_generator(loop, stream):
    reader = asyncio.StreamReader(loop=loop)
    reader_protocol = asyncio.StreamReaderProtocol(reader)
    await loop.connect_read_pipe(lambda: reader_protocol, stream)

    while True:
        line = await reader.readline()
        if not line: break
        yield line.decode('utf-8')

class LiveCollect():
    def __init__(self):
        self.lines = []

    def connect(self, loop):
        async def follow_stream():
            async for line in stream_as_generator(loop, self.stream):
                self.lines.append(line)

        loop.create_task(follow_stream())

    def collect(self):
        # the task is not running during this call
        copy = self.lines
        self.lines = []

        yield from copy


class FollowFile(LiveCollect):
    def __init__(self, machine, path):
        super().__init__()

        self.path = path
        self.machine = machine

    def start(self):
        self.machine.run(f"test -e {self.path}")
        print(f"FOLLOW {self.path}")
        self.tail = self.machine.Process(f"tail -f {self.path}",
                                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.stream = self.tail.stdout

    def stop(self):
        self.tail.terminate()
        self.tail.wait(2)


class LiveStream(LiveCollect):

    def start(self, stream):
        self.stream = stream

    def stop(self):
        pass

class LiveSocket(LiveCollect):
    def __init__(self, sock, async_read):
        super().__init__()

        self.sock = sock
        self.async_read = async_read

    def connect(self, loop):
        async def follow_socket():
            reader, writer = await asyncio.open_connection(sock=self.sock)
            while True:
                entry = await self.async_read(reader)
                if entry is False: # None is allowed
                    break
                self.lines.append(entry)
            print(f"Connection to {self.sock.getpeername()} closed.")
        loop.create_task(follow_socket())
