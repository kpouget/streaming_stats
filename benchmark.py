import re
import importlib
import asyncio
import signal
import traceback
import sys

from experiment import Experiment, NotEnoughHostFramesExperimentException, NoGuestFramesInHostFramesExperimentException
from measurement import ProcessNotRunningMeasurementException

quit_signal = False
def signal_handler(sig, frame):
    global quit_signal
    if quit_signal: return
    #print("\nQuitting ...") # shouldn't print inside the sig handler ...
    quit_signal = True
    loop = asyncio.get_event_loop()
    loop.stop()

signal.signal(signal.SIGINT, signal_handler)

# run a single test
def run_one(cfg, description, total_run_time=False, dry_run=False, fail_safe=False, live=False):
    if dry_run: print("DRY RUN")

    assert not (live and total_run_time)

    experiment = Experiment(cfg, live=live)
    if description:
        experiment.set_param('description', description)

    # load and initialize measurements
    measurements = []
    name_re = re.compile(r'^[a-z_][a-z0-9_]*$', re.I)
    for m in cfg['measurements']:
        cfg = None
        if type(m) == dict:
            l = list(m.keys())
            assert len(l) == 1, 'Invalid measurement specification %s' % m
            cfg = m[l[0]]
            m = l[0]
        if type(m) != str or not name_re.match(m):
            raise Exception('Invalid module name %s' % m)
        mod = importlib.import_module('measurement.' + m.lower())
        measurements.append(getattr(mod, m)(cfg, experiment))

    print("\n* Preparing the environment ...")

    if not dry_run:
        try:
            for m in measurements: m.setup()
        except Exception as e:
            print(f"FATAL: {m.__class__.__name__}: {e}")
            return 1

    print("\n* Starting the measurements ...")
    if not dry_run:
        try:
            for m in measurements: m.start()
        except ProcessNotRunningMeasurementException as e:
            print("FATAL:", m.__class__.__name__, f": Process not running ({e})")
            return

    loop = asyncio.get_event_loop()

    async def timer_kick(wait_time):
        await asyncio.sleep(wait_time)
        loop.stop()

    for m in measurements:
        if not m.live: continue
        if dry_run: continue

        m.live.connect(loop)

    if total_run_time:
        print("\n* Benchmarking during {} seconds ...".format(total_run_time))
    else:
        print("\n* Benchmarking during forever ...")

    def process():
        for m in measurements:
            if not m.live: continue

            for line in m.live.collect():
                m.process_line(line)
    fatal = None
    recording_time = None
    while True:
        FIRST_RECORD_SLOT_TIME = 1 # seconds (will be dropped)
        RECORD_SLOT_TIME = 1 # seconds

        record_time = FIRST_RECORD_SLOT_TIME if recording_time is None else RECORD_SLOT_TIME
        loop.create_task(timer_kick(record_time))

        # returns after timer_kick() calls loop.stop()
        loop.run_forever()

        try:
            process()
        except Exception as e:
            print(f"FATAL: {e.__class__.__name__} raised while processing: {e}")
            fatal = sys.exc_info()
            break

        if recording_time is None:
            print("Dropping the first recording ...")
            # drop the first recording
            recording_time = 0
            experiment.truncate() # successfull save, truncate the recording
            continue

        recording_time += record_time

        try:
            experiment.save(fail_safe=False)
            experiment.truncate() # successfull save, truncate the recording
        except NotEnoughHostFramesExperimentException:
            print("WARNING: not enough host frames were collected, cannot update")
        except NoGuestFramesInHostFramesExperimentException:
            print("WARNING: Cannot find guest frames in host ones, last slot skipped")
            experiment.truncate()
        except Exception as e:
            print(f"FATAL: {e.__class__.__name__} raised while saving: {e}")
            fatal = sys.exc_info()
            global quit_signal
            quit_signal = True

        if quit_signal: break
        if total_run_time and recording_time > total_run_time: break

    print("\n* Stopping the measurements ...")
    for task in asyncio.Task.all_tasks():
        task.cancel()

    for m in measurements: m.stop()

    if not live:
        print("\n* Collecting the results ...")
        for m in measurements:  m.collect()

        print("\n* Saving the results in the database ...")
        # save to database
        experiment.save(fail_safe=args.fail_safe)

    print("\n* Done!")
    if fatal:
        traceback.print_exception(*fatal)
